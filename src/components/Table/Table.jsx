import React from 'react';
import Track from "../Track";
import './Table.css'

const Table = ({table}) => {
if(table.length !== 0){
    return (
        <div className={'table'}>
                <div className={'table-header'}>
                    <span>ID</span>
                    <span>Full Name</span>
                    <span>Phone</span>
                    <span>Email</span>
                    <span>Age</span>
                    <span>Experience</span>
                    <span>Yearly Income</span>
                    <span>Has Children</span>
                    <span>License States</span>
                    <span>Expiration Date</span>
                    <span>License Number</span>
                    <span>Dublicate with</span>
                </div>
                <div className={'table-main'}>
                    <Track params={table}/>
                </div>


        </div>
    );
}else{return null}
}

export default Table;