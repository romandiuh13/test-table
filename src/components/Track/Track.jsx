import React from 'react';
import moment from 'moment';

const Track = ({params}) => {
    const track = []
    params.forEach(item => {
        item['Email'] = item["Email"].toLowerCase()
        item['Has children'] = item["Has children"].toLowerCase()
        if (item['Phone'].length === 10) {
            item['Phone'] = +1 + item['Phone']
        } else if (item['Phone'].length === 11) {
            item['Phone'] = '+' + item['Phone']
        }
    })

    params.forEach((item, index) => {
        const obj = {}
        for (let key in item) {
            const slicedArr = [...params]
            const sliced = slicedArr.splice(index, 1, [])
            if (key === 'Full Name') {
                const style = {backgroundColor: 'rgb(147,195,129)'}
                obj.fullName = <span style={style}>{item[key]}</span>
            }
            if (key === 'Phone') {
                const reg = /^((1|\+1))?[\d\- ]{10}$/
                const error = reg.test(item[key])
                const style = error ? {backgroundColor: 'rgb(147,195,129)'} : {backgroundColor: 'rgb(244, 204,204)'}
                obj.phone = <span style={style}>{item[key]}</span>
                const idx = slicedArr.find((elem, index) => elem['Phone'] === item[key])
                const prod = params.indexOf(idx, 0)
                if (prod > 0) {
                    obj.duplicate = prod +1
                } else if(prod === -1 && !obj.duplicate) {
                    obj.duplicate = 0
                }
            }
            if (key === 'Email') {
                const style = item[key] !== '' ? {backgroundColor: 'rgb(147,195,129)'} : {backgroundColor: 'rgb(244, 204,204)'}
                obj.email = <span style={style}>{item[key]}</span>

                const idx = slicedArr.find((elem, index) => elem['Email'] === item[key])
                const prod = params.indexOf(idx, 0)
                if (prod === -1 && !obj.duplicate) {
                    obj.duplicate = 0
                } else {
                    obj.duplicate = prod + 1
                }
            }
            if (key === 'Age') {
                const style = Number.isInteger(+item[key]) && item[key] > 21 && item[key] > 0 && item[key] !== '' ? {backgroundColor: 'rgb(147,195,129)'} : {backgroundColor: 'rgb(244, 204,204)'}
                obj.age = <span style={style}>{item[key]}</span>
            }
            if (key === 'Experience') {
                const style = item[key] >= 0 && item[key] <= item['Age'] ? {backgroundColor: 'rgb(147,195,129)'} : {backgroundColor: 'rgb(244, 204,204)'}
                obj.experience = <span style={style}>{item[key]}</span>
            }
            if (key === 'Yearly Income') {
                const num = +item[key]
                const current = num.toFixed(2)
                const style = current <= 1000000 && current >= 0 ? {backgroundColor: 'rgb(147,195,129)'} : {backgroundColor: 'rgb(244, 204,204)'}
                obj.yearly = <span style={style}>{current}</span>
            }
            if (key === "Has children") {
                const style = item[key] === 'true' || item[key] === true ? {backgroundColor: 'rgb(147,195,129)'} : {backgroundColor: 'rgb(244, 204,204)'}
                obj.child = <span style={style}>{item[key]}</span>
            }
            if (key === 'License states') {
                const arrStr = item[key].split(',')
                const current = arrStr.map(item => item.trim().substr(0, 2))
                const currentStr = current.join('|')
                const style = {backgroundColor: 'rgb(147,195,129)'}
                obj.licenseStates = <span style={style}>{currentStr}</span>
            }
            if (key === "Expiration date") {
                const format = moment(item[key], "MM-DD-YYYY", true).isValid()
                const format2 = moment(item[key], "MM/DD/YYYY", true).isValid()
                const nowDate = new Date()
                const style = (nowDate < Date.parse(item[key]) && format) || (nowDate < Date.parse(item[key]) && format2) ? {backgroundColor: 'rgb(147,195,129)'} : {backgroundColor: 'rgb(244, 204,204)'}
                obj.expirationDate = <span style={style}>{item[key]}</span>
            }
            if (key === "License number") {
                const reg = /^[A-Za-z0-9 ]{0,}$/;
                const error = reg.test(item[key])
                const style = error && item[key].length === 6 ? {backgroundColor: 'rgb(147,195,129)'} : {backgroundColor: 'rgb(244, 204,204)'}
                obj.licenseNumber = <span style={style}>{item[key]}</span>
            }
        }
        track.push(obj)
    })

    const trackLine = track.map((item, index) => {

        return (<>
                <span>{index + 1}</span>
                {item.fullName}
                {item.phone}
                {item.email}
                {item.age}
                {item.experience}
                {item.yearly}
                {item.child}
                {item.licenseStates}
                {item.expirationDate}
                {item.licenseNumber}
                <span>{item.duplicate}</span>
            </>
        )
    })
    return (
        <>
            {trackLine}
        </>
    );
};

export default Track;