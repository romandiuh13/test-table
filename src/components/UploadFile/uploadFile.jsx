import React, {useState} from 'react'
import Papa from 'papaparse'
import Table from "../Table";

const UploadFile = () => {
    const [state, setState] = useState({
        csvfile: undefined,
        result: []
    })

    const handleChange = event => {
        setState({...state, csvfile: event.target.files[0]});
    };

    const importCSV = () => {
        const {csvfile} = state;
        Papa.parse(csvfile, {
            complete: updateData,
            header: true
        });
    };

    const updateData = (result) => {
        const data = result.data;
        setState({...state, result: data})

    }

    return (
        <div className="App">
            <h2>Выебрите .csv файл!</h2>
            <input
                className="csv-input"
                type="file"
                accept=".csv"
                ref={input => {
                    const filesInput = input;
                }}
                name="file"
                placeholder={null}
                onChange={handleChange}
            />
            <p/>
            <button onClick={importCSV}>Загрузить данные</button>
            <Table table={state.result}/>
        </div>
    );
}


export default UploadFile;